<?php
/**
 * @file
 * smc_blog_feature.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function smc_blog_feature_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['tagadelic_taxonomy-tagadelic_taxonomy'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'tagadelic_taxonomy',
    'module' => 'tagadelic_taxonomy',
    'node_types' => array(),
    'pages' => 'blog-posts
blog-tags/*',
    'roles' => array(),
    'themes' => array(
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'smc_base' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'smc_base',
        'weight' => -9,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-blog_posts-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'blog_posts-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'blog-posts
blog-tags/*',
    'roles' => array(
      'administrator' => 3,
      'authenticated user' => 2,
    ),
    'themes' => array(
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'smc_base' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'smc_base',
        'weight' => -10,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-recent_comments-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'recent_comments-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'blog-posts
blog-tags/*',
    'roles' => array(),
    'themes' => array(
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'smc_base' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'smc_base',
        'weight' => -12,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
