<?php
/**
 * @file
 * smc_blog_feature.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function smc_blog_feature_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_blog:blog-posts
  $menu_links['main-menu_blog:blog-posts'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'blog-posts',
    'router_path' => 'blog-posts',
    'link_title' => 'Blog',
    'options' => array(
      'identifier' => 'main-menu_blog:blog-posts',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Blog');


  return $menu_links;
}
